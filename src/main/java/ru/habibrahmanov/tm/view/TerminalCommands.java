package ru.habibrahmanov.tm.view;

public class TerminalCommands {

    public static final String PROJECTCREATE = "project-create";
    public static final String PCR = "pcr";

    public static final String PROJECTLIST = "project-list";
    public static final String PL = "pl";

    public static final String PROJECTREMOVE = "project-remove";
    public static final String PR = "pr";

    public static final String PROJECTEDIT = "project-edit";
    public static final String PE = "pe";

    public static final String PROJECLEAR = "project-clear";
    public static final String PCL = "pcl";

    public static final String PROJECTMERGE = "project-merge";
    public static final String PM = "pm";

    public static final String TASKCREATE = "task-create";
    public static final String TCR = "tcr";

    public static final String TASKCLIST = "task-list";
    public static final String TL = "tl";

    public static final String SHOWALLTASK = "show-all-task";
    public static final String SAT = "sat";

    public static final String TASKCREMOVE = "task-remove";
    public static final String TR = "tr";

    public static final String TASKCEDIT = "task-edit";
    public static final String TE = "te";

    public static final String TASKMERGE = "task-merge";
    public static final String TM = "tm";

    public static final String HELP = "help";

}
