package ru.habibrahmanov.tm.view;

import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.repositiry.ProjectRepository;
import ru.habibrahmanov.tm.repositiry.TaskRepository;
import ru.habibrahmanov.tm.service.ProjectService;
import ru.habibrahmanov.tm.service.TaskService;
import java.util.Scanner;

public class Bootstrap {
    private ProjectRepository projectRepository = new ProjectRepository();
    private TaskRepository taskRepository = new TaskRepository();
    private ProjectService projectService = new ProjectService(projectRepository);
    private TaskService taskService = new TaskService(taskRepository);
    private Scanner scanner = new Scanner(System.in);
    private String sc;

    private Menu menu = new Menu(projectService, projectRepository, taskService, taskRepository, scanner);

    public void init() throws IncorrectValueException {
        System.out.println("***WELCOME TO TASK MANAGER***");
        System.out.println("Enter HELP for show all commands.");

        do {
            sc = scanner.nextLine();

            switch (sc.toLowerCase()) {
                case (TerminalCommands.PROJECTCREATE):
                case (TerminalCommands.PCR):
                    menu.commandCreatePtoject();
                    break;

                case (TerminalCommands.PROJECTLIST):
                case (TerminalCommands.PL):
                    menu.commandShowProject();
                    break;

                case (TerminalCommands.SHOWALLTASK):
                case (TerminalCommands.SAT):
                    menu.commandShowAllTask();
                    break;

                case (TerminalCommands.PROJECTREMOVE):
                case (TerminalCommands.PR):
                    menu.commandRemoveProject();
                    break;

                case (TerminalCommands.PROJECTEDIT):
                case (TerminalCommands.PE):
                    menu.commandEditProject();
                    break;

                case (TerminalCommands.PROJECLEAR):
                case (TerminalCommands.PCL):
                    menu.commandClearProject();
                    break;

                case (TerminalCommands.PROJECTMERGE):
                case (TerminalCommands.PM):
                    menu.commandMergeProject();
                    break;

                case (TerminalCommands.TASKCREATE):
                case (TerminalCommands.TCR):
                    menu.commandCreateTask();
                    break;

                case (TerminalCommands.TASKCLIST):
                case (TerminalCommands.TL):
                    menu.commandShowTask();
                    break;

                case (TerminalCommands.TASKCREMOVE):
                case (TerminalCommands.TR):
                    menu.commandRemoveTask();
                    break;

                case (TerminalCommands.TASKCEDIT):
                case (TerminalCommands.TE):
                    menu.commandEditTask();
                    break;

                case (TerminalCommands.TASKMERGE):
                case (TerminalCommands.TM):
                    menu.commandMergeTask();
                    break;

                case (TerminalCommands.HELP):
                    menu.commandHelpProject();
                    break;
            }
        } while (!sc.equals("exit"));
    }
}
