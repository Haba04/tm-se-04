package ru.habibrahmanov.tm.view;

import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.repositiry.ProjectRepository;
import ru.habibrahmanov.tm.repositiry.TaskRepository;
import ru.habibrahmanov.tm.service.ProjectService;
import ru.habibrahmanov.tm.service.TaskService;
import java.util.Map;
import java.util.Scanner;

public class Menu {

    private ProjectService projectService;
    private ProjectRepository projectRepository;
    private TaskService taskService;
    private TaskRepository taskRepository;
    private Scanner scanner;
    private String projectId;
    private String taskId;
    private String name;

    public Menu(ProjectService projectService, ProjectRepository projectRepository, TaskService taskService, TaskRepository taskRepository, Scanner scanner) {
        this.projectService = projectService;
        this.projectRepository = projectRepository;
        this.taskService = taskService;
        this.taskRepository = taskRepository;
        this.scanner = scanner;
    }

    //persist (ProjectRepository)
    public void commandCreatePtoject() throws IncorrectValueException {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        name = scanner.nextLine();
        projectService.persist(name);
        System.out.println("CREATE NEW PROJECT SUCCESSFULLY");
    }

    public void commandShowProject() throws IncorrectValueException {
        System.out.println("[PROJECT LIST]");
        projectService.findAll();
        for (Map.Entry<String, Project> entry : projectRepository.findAll().entrySet()) {
            System.out.println("PROJECT ID: " + entry.getValue().getId() + " / NAME: " + entry.getValue().getName());
        }
    }

    public void commandRemoveProject() throws IncorrectValueException {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER ID PROJECT:");
        projectId = scanner.nextLine();
        projectService.removeOne(projectId);
        taskService.removeAll(projectId);
        System.out.println("PROJECT REMOVED SUCCESSFULLY");
    }

    public void commandEditProject() throws IncorrectValueException {
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER ID");
        projectId = scanner.nextLine();
        System.out.println("ENTER NAME TO CHANGE");
        name = scanner.nextLine();
        projectService.update(projectId, name);
        System.out.println("PROJECT EDIT SUCCESSFULLY");
    }

    public void commandClearProject() throws IncorrectValueException {
        System.out.println("[PROJECT CLEAR]");
        projectService.removeAll();
        System.out.println("DELETE ALL PROJECTS");
    }

    public void commandMergeProject() throws IncorrectValueException {
        System.out.println("[PROJECT MERGE]");
        System.out.println("ENTER PROJECT ID");
        projectId = scanner.nextLine();
        System.out.println("ENTER PROJECT NAME");
        name = scanner.nextLine();
        projectService.merge(projectId, name);
        System.out.println("PROJECT MERGE SUCCESSFULLY");
    }

    public void commandCreateTask() throws IncorrectValueException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER PROJECT ID");
        projectId = scanner.nextLine();
        System.out.println("ENTER TASK NAME");
        name = scanner.nextLine();
        taskService.persist(projectId, name);
        System.out.println("TASK CREATE SUCCESSFULLY");
    }

    public void commandShowTask() throws IncorrectValueException {
        System.out.println("[TASK LIST]");
        System.out.println("[ENTER TASK ID]");
        taskId = scanner.nextLine();
        taskService.findOne(taskId);

        for (Map.Entry<String, Task> entry : taskRepository.findAll().entrySet()) {
            System.out.println("TASK ID: " + entry.getValue().getId() + " / NAME: " + entry.getValue().getName());
        }
    }

    public void commandShowAllTask() throws IncorrectValueException {
        System.out.println("[SHOW ALL TASK]");
        taskService.findAll();
        for (Map.Entry<String, Task> entry : taskService.findAll().entrySet()) {
            System.out.println("ID: " + entry.getKey() + " / NAME: " + entry.getValue().getName());
        }
    }

    public void commandRemoveTask() throws IncorrectValueException {
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER ID TASK:");
        taskId = scanner.nextLine();
        taskService.removeOne(taskId);
        System.out.println("TASK REMOVED SUCCESSFULLY");
    }

    public void commandEditTask() throws IncorrectValueException {
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER TASK ID:");
        taskId = scanner.nextLine();
        System.out.println("ENTER TASK NAME:");
        name = scanner.nextLine();
        taskService.update(taskId, name);
        System.out.println("TASK UPDATED SUCCESSFULLY");
    }

    public void commandMergeTask() throws IncorrectValueException {
        System.out.println("[TASK MERGE]");
        System.out.println("ENTER PROJECT ID:");
        projectId = scanner.nextLine();
        System.out.println("ENTER TASK ID:");
        taskId = scanner.nextLine();
        System.out.println("ENTER TASK NAME:");
        name = scanner.nextLine();
        taskService.merge(projectId, taskId, name);
        System.out.println("TASK MERGE SUCCESSFULLY");
    }

    public void commandHelpProject() {
        System.out.println("help: Show all commands");
        System.out.println("[pcl] project-clear: Remove all project");
        System.out.println("[pr] project-remove: Remove selected project");
        System.out.println("[pcr] project-create: Create new projects");
        System.out.println("[pl] project-list: Shaw all project");
        System.out.println("[ps] project-switch: Switch current project");
        System.out.println("[tcr] task-create: Create new task");
        System.out.println("[tl] task-list: Show all tasks");
        System.out.println("[tr] task-remove: Remove selected task");
        System.out.println("[te] task-edit: Edit selected task");
    }

}
