package ru.habibrahmanov.tm;

import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.view.Bootstrap;

public class Application {

    public static void main(String[] args) throws IncorrectValueException {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
