package ru.habibrahmanov.tm.service;

import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.repositiry.TaskRepository;
import java.util.Map;
import java.util.UUID;

public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void persist(String projectId, String name) throws IncorrectValueException {
        if (projectId == null || projectId.isEmpty()) {
            throw new IncorrectValueException();
        }
        taskRepository.persist(new Task(name, UUID.randomUUID().toString(), projectId));
    }

    public void findOne(String taskId) throws IncorrectValueException {
        if (taskId == null || taskId.isEmpty()) {
            throw new IncorrectValueException();
        }
        taskRepository.findOne(taskId);
    }

    public Map<String, Task> findAll() throws IncorrectValueException {
        if (taskRepository.findAll().isEmpty()) {
            throw new IncorrectValueException("LIST EMPTY");
        }
        return taskRepository.findAll();
    }

    public void removeOne(String taskId) throws IncorrectValueException {
        if (taskId == null || taskId.isEmpty()) {
            throw new IncorrectValueException();
        }
        taskRepository.removeOne(taskId);
    }

    public void removeAll(String projectId) {
        taskRepository.removeAll(projectId);
    }

    public void update(String taskId, String name) throws IncorrectValueException {
        if (taskId == null || taskId.isEmpty() || name == null || name.isEmpty()) {
            throw new IncorrectValueException();
        }
        taskRepository.update(taskId, name);
    }

    public void merge(String projectId, String taskId, String name) throws IncorrectValueException {
        if (taskId == null || taskId.isEmpty() || name == null || name.isEmpty()) {
            throw new IncorrectValueException();
        }
        taskRepository.merge(new Task(name, taskId, projectId));
    }
}
