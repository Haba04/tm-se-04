package ru.habibrahmanov.tm.service;

import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.repositiry.ProjectRepository;
import java.util.Map;
import java.util.UUID;

public class ProjectService {
    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void persist(String projectName) throws IncorrectValueException {
        if (projectName == null || projectName.isEmpty()){
            throw new IncorrectValueException();
        }
        projectRepository.persist(new Project(projectName, UUID.randomUUID().toString()));
    }

    public Map<String, Project> findAll() throws IncorrectValueException {
        if (projectRepository.findAll().isEmpty()) {
            throw new IncorrectValueException("PROJECT LIST EMPTY");
        }
        return projectRepository.findAll();
    }

    public void removeAll() throws IncorrectValueException {
        if (projectRepository.getProjectMap().isEmpty()) {
            throw new IncorrectValueException("PROJECT LIST EMPTY");
        }
        projectRepository.removeAll();
    }

    public void removeOne(String projectId) throws IncorrectValueException {
        if (projectId == null || projectId.isEmpty()) {
            throw new IncorrectValueException();
        }
        projectRepository.removeOne(projectId);
    }

    public void update(String projectId, String name) throws IncorrectValueException {
        if (projectId == null || projectId.isEmpty() || name == null || name.isEmpty()) {
            throw new IncorrectValueException();
        }
        projectRepository.update(projectId, name);
    }

    public void merge(String projectId, String name) throws IncorrectValueException {
        if (projectId == null || projectId.isEmpty() || name == null || name.isEmpty()) {
            throw new IncorrectValueException();
        }
        projectRepository.merge(new Project(name, projectId));
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }
}
